
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { SignUp } from './components/SignUp'
// import artificialImg from './assets/images/ai.jpg';
import artificialImage from './assets/images/artificial_img.png';


function App() {
  return (
    <div className='container mt-3'>
      <div className='row'>
        <div className='col-md-5'>
          <SignUp />
        </div> 
        <div className='col-md-7'>
          <img className="img-fluid w-100" src={artificialImage} alt="artificial intelligence" />
        </div>
      </div>
    </div>
  );
}

export default App;
